using System;
using UnityEngine;

namespace MeshGeneration
{
    public struct TransformValueCache : IEquatable<Transform>
    {
        private readonly Vector3 position;
        private readonly Vector3 localScale;
        private readonly Quaternion rotation;

        public TransformValueCache(Transform transform)
        {
            position = transform.position;
            localScale = transform.localScale;
            rotation = transform.rotation;
        }

        public bool Equals(Transform other)
        {
            if (other == null)
            {
                return false;
            }
            return position.Equals(other.position) && localScale.Equals(other.localScale) && rotation.Equals(other.rotation);
        }
    }
    
    [ExecuteAlways]
    public class MeshRegenerationTrigger : MonoBehaviour
    {
        [SerializeField] private OctreeMeshGen octreeMeshGen;
        [SerializeField] private OctreeMeshGeneratorProperties octreeMeshGeneratorProperties;

        private int octreeMeshGenPropertiesMaxDepthCache;
        
        private SDFObject octreeMeshGeneratorPropertiesSDFCache;
        private TransformValueCache octreeMeshGeneratorPropertiesSDFTransformCache;
        
        private TransformValueCache octreeMeshGenTransformCache;

        private void Start()
        {
            octreeMeshGenPropertiesMaxDepthCache = octreeMeshGeneratorProperties.MaxOctreeDepth;
            octreeMeshGeneratorPropertiesSDFCache = octreeMeshGeneratorProperties.sdf;
            octreeMeshGeneratorPropertiesSDFTransformCache = new TransformValueCache(octreeMeshGeneratorProperties.sdf.transform);
            octreeMeshGenTransformCache = new TransformValueCache(octreeMeshGen.transform);
            
            octreeMeshGen.RegenerateGeometry();
        }

        private void Update()
        {
            bool dirty = false;

            if (octreeMeshGenPropertiesMaxDepthCache != octreeMeshGeneratorProperties.MaxOctreeDepth)
            {
                Debug.Log("UpdateMaxDepth should recalculate octree");
                dirty = true;
                octreeMeshGenPropertiesMaxDepthCache = octreeMeshGeneratorProperties.MaxOctreeDepth;
            }

            if (octreeMeshGeneratorPropertiesSDFCache != octreeMeshGeneratorProperties.sdf)
            {
                Debug.Log("Change SDF Object should recalculate octree");
                dirty = true;
                octreeMeshGeneratorPropertiesSDFCache = octreeMeshGeneratorProperties.sdf;
            }

            if (!octreeMeshGeneratorPropertiesSDFTransformCache.Equals(octreeMeshGeneratorProperties.sdf.transform))
            {
                Debug.Log("Change SDF Transform should recalculate octree");
                dirty = true;
                octreeMeshGeneratorPropertiesSDFTransformCache = new TransformValueCache(octreeMeshGeneratorProperties.sdf.transform);
            }

            if (!octreeMeshGenTransformCache.Equals(octreeMeshGen.transform))
            {
                Debug.Log("Change SDF Object should recalculate octree");
                dirty = true;
                octreeMeshGenTransformCache = new TransformValueCache(octreeMeshGen.transform);
            }
            
            if (dirty)
            {
                octreeMeshGen.RegenerateGeometry();
            }
        }
    }
}