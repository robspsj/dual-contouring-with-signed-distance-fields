﻿using System.Collections.Generic;


namespace MeshGeneration
{
    internal struct Axis
    {
        private readonly int value;
        internal Axis(int value)
        {
            this.value = value;
        }


        public override string ToString()
        {
            string formated = "";
            if ((value & 1) != 0) formated = "X";
            if ((value & 2) != 0) formated += "Y";
            if ((value & 4) != 0) formated += "Z";
            return !string.IsNullOrEmpty(formated) ? formated : "---";
        }
        public static implicit operator int(Axis a) => a.value;
        public static implicit operator Axis(int a) => new Axis(a);

        public const int X = 1;
        public const int Y = 2;
        public const int Z = 4;
        public const int XY = 1+2;
        public const int YZ = 2+4;
        public const int XZ = 1+4;
        public const int XYZ = 1+2+4;
        

        public void ComplementaryAxes(out Axis c1, out Axis c2)
        {
            if (value == X)
            {
                c1 = Y;
                c2 = Z;
            }
            else if (value == Y)
            {
                c1 = Z;
                c2 = X;
            }
            else
            {
                c1 = X;
                c2 = Y;
            }
        }

        public static IEnumerable<Axis> All()
        {
            yield return X;
            yield return Y;
            yield return Z;
        }
    }
}