﻿using System.Collections.Generic;
using System.Threading;
using UnityEngine;



[ExecuteAlways]
public class CubicMeshGen : MonoBehaviour
{
    public SDFObject sdf;
    private readonly List<Vector3> vertices = new List<Vector3>();
    private readonly List<int> triangles = new List<int>();

    public Vector3 geometryArea = Vector3.one * 2;
    public Vector3Int SegmentCount = new Vector3Int(16, 16, 16);
    public bool resetOnUpdate;

    private Vector3 previousGeometryArea;
    private Vector3Int previousSegmentCount;
    private Vector3 previousPosition;

    private Vector3 cellStep;
    private Vector3 xStep;
    private Vector3 yStep;
    private Vector3 cellStartPos;
    private Vector3 zStep;

    private Vector3 geometryCenter;

    private CancellationTokenSource cancellationTokenSource;
    
    private void OnEnable()
    {
        GenerateMeshes();
    }

    private void Update()
    {
        if (resetOnUpdate)
        {
            if (previousGeometryArea != geometryArea ||
                previousPosition != transform.position ||
                previousSegmentCount != SegmentCount||
                sdf.IsDirty)
            {
                GenerateMeshes();
            }
        }
    }

    private void GenerateMeshes()
    {
        var watch = System.Diagnostics.Stopwatch.StartNew();

        CacheSettings();
        
        vertices.Clear();
        triangles.Clear();
        
        GenerateGeometry();
        CreateMeshAndSet();


        watch.Stop();
        Debug.Log($"[Time] ({SegmentCount})({SegmentCount.x * SegmentCount.y * SegmentCount.z}) " +
                  $"segments calculated  in {watch.ElapsedMilliseconds} ms");
    }
    private void CacheSettings()
    {
        previousGeometryArea = geometryArea;
        previousPosition = transform.position;
        previousSegmentCount = SegmentCount;
    }

    private void GenerateGeometry()
    {
        geometryCenter = previousPosition;

        cellStartPos = geometryCenter - geometryArea * 0.5f;
        cellStep = new Vector3(geometryArea.x / SegmentCount.x, geometryArea.y / SegmentCount.y,
            geometryArea.z / SegmentCount.z);
        xStep = new Vector3(cellStep.x, 0, 0);
        yStep = new Vector3(0, cellStep.y, 0);
        zStep = new Vector3(0, 0, cellStep.z);


        for (var x = 0; x < SegmentCount.x; x++)
        {
            for (var y = 0; y < SegmentCount.y; y++)
            {
                for (var z = 0; z < SegmentCount.z; z++)
                {
                    float valueOrigin = sdf.GetPointValue(CalculateCellOrigin(new Vector3Int(x + 0, y + 0, z + 0)));
                    float valuePlusX = sdf.GetPointValue(CalculateCellOrigin(new Vector3Int(x + 1, y + 0, z + 0)));
                    float valuePlusY = sdf.GetPointValue(CalculateCellOrigin(new Vector3Int(x + 0, y + 1, z + 0)));
                    float valuePlusZ = sdf.GetPointValue(CalculateCellOrigin(new Vector3Int(x + 0, y + 0, z + 1)));

                    TryGenerateQuadForEdge(new Vector3Int(x, y, z), new Vector3Int(1, 0, 0), valueOrigin, valuePlusX);
                    TryGenerateQuadForEdge(new Vector3Int(x, y, z), new Vector3Int(0, 1, 0), valueOrigin, valuePlusY);
                    TryGenerateQuadForEdge(new Vector3Int(x, y, z), new Vector3Int(0, 0, 1), valueOrigin, valuePlusZ);
                }
            }
        }


        // point placement should be made elsewhere
        
        for (var i = 0; i < vertices.Count; i++)
        {
            Vector3 vertexWorldPos = vertices[i] + geometryCenter;
            
            float pointValue = sdf.GetPointValue(vertexWorldPos);
            Vector3 gradient = sdf.GetGradient(vertexWorldPos);
            
            Vector3 gradientDirection = gradient / gradient.magnitude;
            vertices[i] = vertices[i] - gradientDirection * pointValue; 
        }
    }

    private Vector3 CalculateCellOrigin(Vector3Int cellAddress)
    {
        return cellStartPos + cellAddress.x * xStep + cellAddress.y * yStep + cellAddress.z * zStep;
    }

    private void TryGenerateQuadForEdge(Vector3Int originCellAddress, Vector3Int testAxis, float v0Value, float v1Value)
    {
        if (v0Value * v1Value < 0 || (v0Value * v1Value == 0 && v0Value + v1Value < 0))
        {
            GenerateQuadForEdge(originCellAddress, testAxis);
        }
    }
    private void GenerateQuadForEdge(Vector3Int corner, Vector3Int testAxis)
    {
        Vector3Int side0 = default;
        Vector3Int side1 = default;

        if (testAxis == new Vector3Int(1, 0, 0))
        {
            side0 = new Vector3Int(0, -1, 0);
            side1 = new Vector3Int(0, 0, -1);
        }
        else if (testAxis == new Vector3Int(0, 1, 0))
        {
            side0 = new Vector3Int(-1, 0, 0);
            side1 = new Vector3Int(0, 0, -1);
        }
        else if (testAxis == new Vector3Int(0, 0, 1))
        {
            side0 = new Vector3Int(-1, 0, 0);
            side1 = new Vector3Int(0, -1, 0);
        }


        GenerateQuadFromCorner(corner, side0, side1);
    }

    private void GenerateQuadFromCorner(Vector3Int corner, Vector3Int side0, Vector3Int side1)
    {
        int v0 = vertices.Count;
        int v1 = v0 + 1;
        int v2 = v0 + 2;
        int v3 = v0 + 3;

        Vector3 vertex0 = CalculateCellOrigin(corner) + cellStep * 0.5f - geometryCenter;
        Vector3 vertex1 = CalculateCellOrigin(corner + side0) + cellStep * 0.5f - geometryCenter;
        Vector3 vertex2 = CalculateCellOrigin(corner + side0 + side1) + cellStep * 0.5f - geometryCenter;
        Vector3 vertex3 = CalculateCellOrigin(corner + side1) + cellStep * 0.5f - geometryCenter;

        vertices.Add(vertex0);
        vertices.Add(vertex1);
        vertices.Add(vertex2);
        vertices.Add(vertex3);

        Vector3 normal = Vector3.Cross(vertex1 - vertex0, vertex2 - vertex0);
        Vector3 gradient = sdf.GetGradient(CalculateCellOrigin(corner) + cellStep * 0.5f);

        if  (Vector3.Dot(normal, gradient) >= 0)
        {
            triangles.Add(v0);
            triangles.Add(v1);
            triangles.Add(v3);

            triangles.Add(v3);
            triangles.Add(v1);
            triangles.Add(v2);
        }
        else
        {
            triangles.Add(v1);
            triangles.Add(v0);
            triangles.Add(v3);

            triangles.Add(v3);
            triangles.Add(v2);
            triangles.Add(v1);
        }
    }


    private void CreateMeshAndSet()
    {
        var meshFilter = GetComponent<MeshFilter>();
        var mesh = new Mesh();
        meshFilter.mesh = mesh;

        mesh.Clear();
        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.RecalculateNormals();
    }
    
    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(transform.position, geometryArea);
    }

}