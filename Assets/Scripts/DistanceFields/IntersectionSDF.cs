﻿using UnityEngine;


[ExecuteAlways]
public class IntersectionSDF : SDFObject
{
    [SerializeField] private SDFObject sdf1;
    [SerializeField] private SDFObject sdf2;

    private void Update()
    {
        Dirty = sdf1.IsDirty || sdf2.IsDirty;
    }

    public override float GetPointValue(Vector3 pos)
    {
        float pointValue1 = sdf1.GetPointValue(pos);
        float pointValue2 = sdf2.GetPointValue(pos);

        if (Mathf.Abs(pointValue1 - pointValue2) < 0.1)
        {
            return Mathf.Lerp(Mathf.Min(pointValue1, pointValue2), Mathf.Max(pointValue1, pointValue2),
                Mathf.Abs(pointValue1 - pointValue2) * 10);
        }
        
        return Mathf.Max(pointValue1, pointValue2);
    }
}