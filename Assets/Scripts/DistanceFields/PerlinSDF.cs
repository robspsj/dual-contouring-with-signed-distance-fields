﻿using UnityEngine;

[ExecuteAlways]
public class PerlinSDF : SDFObject
{
    [Range(-1, 1)]
    public float gain;
    private float gainCache;
    private void Update()
    {
        if (gainCache - gain > 10 * float.Epsilon)
        {
            Dirty = true;
            gainCache = gain;
        }


        if (transformCache.hasChanged)
        {
            Dirty = true;
        }
    }


    public override float GetPointValue(Vector3 pos)
    {
        return Perlin.Noise(transformCache.InverseTransformPoint(pos)) + gain;
    }
}