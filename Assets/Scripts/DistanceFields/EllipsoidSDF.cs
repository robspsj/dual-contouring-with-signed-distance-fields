﻿using UnityEngine;


[ExecuteAlways]
public class EllipsoidSDF : SDFObject
{
    private void Update()
    {
        if (transformCache.hasChanged)
        {
            Dirty = true;
        }
    }


    public override float GetPointValue(Vector3 pos)
    {
        return transformCache.InverseTransformPoint(pos).magnitude - 1;
    }

    void OnDrawGizmos()
    {
        const int segments = 30;

        Gizmos.color = Color.green;

        for (int i = 0; i < segments; i++)
        {
            float angle = i * Mathf.PI * 2 / segments;
            float angle2 = (i + 1) * Mathf.PI * 2 / segments;
            float sin1 = Mathf.Sin(angle);
            float cos1 = Mathf.Cos(angle);
            float sin2 = Mathf.Sin(angle2);
            float cos2 = Mathf.Cos(angle2);
            Gizmos.DrawLine(
                transformCache.TransformPoint(new Vector3(sin1, cos1, 0)),
                transformCache.TransformPoint(new Vector3(sin2, cos2, 0)));
            Gizmos.DrawLine(
                transformCache.TransformPoint(new Vector3(sin1, 0, cos1)),
                transformCache.TransformPoint(new Vector3(sin2, 0, cos2)));
            Gizmos.DrawLine(
                transformCache.TransformPoint(new Vector3(0, sin1, cos1)),
                transformCache.TransformPoint(new Vector3(0, sin2, cos2)));
        }
    }
}