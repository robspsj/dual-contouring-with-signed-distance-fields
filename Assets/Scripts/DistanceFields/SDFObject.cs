﻿using UnityEngine;


[ExecuteAlways]
public abstract class SDFObject : MonoBehaviour
{
    private Transform _transformCache;
    public Transform transformCache
    {
        get {
            if (_transformCache == null)
            {
                _transformCache = transform;
            }


            return _transformCache;
        }
    }

    public abstract float GetPointValue(Vector3 pos);

    protected bool Dirty;
    public bool IsDirty => Dirty;

    public Vector3 GetGradient(Vector3 pos, float d = 0.1f)
    {
        float gradX = (GetPointValue(pos + d * new Vector3(1, 0, 0)) -
                       GetPointValue(pos + d * new Vector3(-1, 0, 0))) / 2 * d;

        float gradY = (GetPointValue(pos + d * new Vector3(0, 1, 0)) -
                       GetPointValue(pos + d * new Vector3(0, -1, 0))) / 2 * d;

        float gradZ = (GetPointValue(pos + d * new Vector3(0, 0, 1)) -
                       GetPointValue(pos + d * new Vector3(0, 0, -1))) / 2 * d;
        return new Vector3(gradX, gradY, gradZ);
    }
}