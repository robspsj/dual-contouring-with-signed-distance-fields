﻿using System.Collections.Generic;
using MeshGeneration.Octree;
using UnityEngine;


namespace MeshGeneration
{
    public class RecursiveOctreePolygonGenerator
    {
        public readonly List<Vector3> Vertices = new();
        public readonly List<int> Triangles = new();

        public Matrix4x4 WorldToLocal = Matrix4x4.identity;

        //todo redo this;
        public List<OctreeNode<OctreeVertexData>> processedNodes;

        public void ProcessCell(OctreeNode<OctreeVertexData> node) // node
        {
            processedNodes = new List<OctreeNode<OctreeVertexData>>() { node };

            if (node.IsLeaf)
            {
                return;
            }


            //ProcessEdge for 6 internal Edges
            foreach (Axis c0 in Axis.All())
            {
                c0.ComplementaryAxes(out Axis c1, out Axis c2);
                
                OctreeNode<OctreeVertexData> n1Edge;
                OctreeNode<OctreeVertexData> n2Edge;
                OctreeNode<OctreeVertexData> n3Edge;
                OctreeNode<OctreeVertexData> n4Edge;
            
                n1Edge = node[0];
                n2Edge = node[c1];
                n3Edge = node[c1 + c2];
                n4Edge = node[c2];
                ProcessEdge(n1Edge, n2Edge, n3Edge, n4Edge, c0);
                
                n1Edge = node[c0 + 0];
                n2Edge = node[c0 + c1];
                n3Edge = node[c0 + c1 + c2];
                n4Edge = node[c0 + c2];
                ProcessEdge(n1Edge, n2Edge, n3Edge, n4Edge, c0);
            }


            //ProcessCell for 8 children
            foreach (OctreeNode<OctreeVertexData> child in node.Children)
            {
                ProcessCell(child);
            }


            //ProcessFace for 12 internal Faces
            foreach (Axis a0 in Axis.All())
            {
                a0.ComplementaryAxes(out Axis a1, out Axis a2);

                for (var i1 = 0; i1 < 2; i1++)
                for (var i2 = 0; i2 < 2; i2++)
                {
                    ProcessFace(node[a1 * i1 + a2 * i2], node[a0 + a1 * i1 + a2 * i2], a0);
                }
            }
        }
        

        private void ProcessFace(OctreeNode<OctreeVertexData> node1, OctreeNode<OctreeVertexData> node2, Axis c0) //(axis is normal to the face)
        {
            processedNodes = new List<OctreeNode<OctreeVertexData>>() { node1, node2 };
            

            // return both are leaves
            if (node1.IsLeaf && node2.IsLeaf)
            {
                return;
            }


            c0.ComplementaryAxes(out Axis c1, out Axis c2);

            if (node1.IsLeaf)
            {
                ProcessFace(node1, node2[0], c0);
                ProcessFace(node1, node2[c1], c0);
                ProcessFace(node1, node2[c2], c0);
                ProcessFace(node1, node2[c1 + c2], c0);

                ProcessEdgeAdaptive(node1, 0, node2[0], c0 + c2, node2[c2], c0, c1);
                ProcessEdgeAdaptive(node1, 0, node2[c1], c0 + c2, node2[c1 + c2], c0, c1);

                ProcessEdgeAdaptive(node1, 0, node2[0], c0 + c1, node2[c1], c0, c2);
                ProcessEdgeAdaptive(node1, 0, node2[c2], c0 + c1, node2[c1 + c2], c0, c2);
                return;
            }


            if (node2.IsLeaf)
            {
                ProcessFace(node1[c0], node2, c0);
                ProcessFace(node1[c0 + c1], node2, c0);
                ProcessFace(node1[c0 + c1 + c2], node2, c0);
                ProcessFace(node1[c0 + c2], node2, c0);

                ProcessEdgeAdaptive(node1[c0], c2, node1[c0 + c2], 0, node2, 0, c1);
                ProcessEdgeAdaptive(node1[c0 + c1], c2, node1[c0 + c1 + c2], 0, node2, 0, c1);

                ProcessEdgeAdaptive(node1[c0], c1, node1[c0 + c1], 0, node2, 0, c2);
                ProcessEdgeAdaptive(node1[c0 + c2], c1, node1[c0 + c1 + c2], 0, node2, 0, c2);
                return;
            }


            ProcessFace(node1[c0], node2[0], c0);
            ProcessFace(node1[c0 + c1], node2[c1], c0);
            ProcessFace(node1[c0 + c1 + c2], node2[c1 + c2], c0);
            ProcessFace(node1[c0 + c2], node2[c2], c0);
            
            // process edges
            
            OctreeNode<OctreeVertexData> n1Edge;
            OctreeNode<OctreeVertexData> n2Edge;
            OctreeNode<OctreeVertexData> n3Edge;
            OctreeNode<OctreeVertexData> n4Edge;
            
            n1Edge = node1[c0];
            n2Edge = node1[c0 + c2];
            n3Edge = node2[c2];
            n4Edge = node2[0];
            ProcessEdge(n1Edge, n2Edge, n3Edge, n4Edge, c1);
            
            
            n1Edge = node1[c0 + c1];
            n2Edge = node1[c0 + c2 + c1];
            n3Edge = node2[c2 + c1];
            n4Edge = node2[0 + c1];
            ProcessEdge(n1Edge, n2Edge, n3Edge, n4Edge, c1);
            
            n1Edge = node1[c0];
            n2Edge = node2[0];
            n3Edge = node2[c1];
            n4Edge = node1[c1 + c0];
            ProcessEdge(n1Edge, n2Edge, n3Edge, n4Edge, c2);
            
            n1Edge = node1[c0+ c2];
            n2Edge = node2[0+ c2];
            n3Edge = node2[c1+ c2];
            n4Edge = node1[c1 + c0 + c2];
            ProcessEdge(n1Edge, n2Edge, n3Edge, n4Edge, c2);
        }


        private void ProcessEdge(OctreeNode<OctreeVertexData> node1,
            OctreeNode<OctreeVertexData> node2,
            OctreeNode<OctreeVertexData> node3,
            OctreeNode<OctreeVertexData> node4,
            Axis c0) //(axis is parallel to edge)
        {
            c0.ComplementaryAxes(out Axis c1, out Axis c2);
            Axis a1 = c1+c2;
            Axis a2 = c2;
            Axis a3 = 0;
            Axis a4 = c1;

            processedNodes = new List<OctreeNode<OctreeVertexData>>() { node1, node2, node3, node4 };

            if (node1.IsLeaf && node2.IsLeaf && node3.IsLeaf && node4.IsLeaf)
            {
                CheckAndGenerateGeometry(node1, node2, node3, node4, c0);
                return;
            }


            OctreeNode<OctreeVertexData> sub1 = node1.IsLeaf ? node1 : node1[a1];
            OctreeNode<OctreeVertexData> sub2 = node2.IsLeaf ? node2 : node2[a2];
            OctreeNode<OctreeVertexData> sub3 = node3.IsLeaf ? node3 : node3[a3];
            OctreeNode<OctreeVertexData> sub4 = node4.IsLeaf ? node4 : node4[a4];
            ProcessEdge(sub1, sub2, sub3, sub4, c0);

            sub1 = node1.IsLeaf ? node1 : node1[a1 + c0];
            sub2 = node2.IsLeaf ? node2 : node2[a2 + c0];
            sub3 = node3.IsLeaf ? node3 : node3[a3 + c0];
            sub4 = node4.IsLeaf ? node4 : node4[a4 + c0];
            ProcessEdge(sub1, sub2, sub3, sub4, c0);
        }

        private void ProcessEdgeAdaptive(OctreeNode<OctreeVertexData> node1, Axis a1,
            OctreeNode<OctreeVertexData> node2, Axis a2,
            OctreeNode<OctreeVertexData> node3, Axis a3,
            Axis c0)
        {
            processedNodes = new List<OctreeNode<OctreeVertexData>>() { node1, node2, node3 };

            if (node1.IsLeaf && node2.IsLeaf && node3.IsLeaf)
            {
                CheckAndGenerateGeometry(node1, a1, node2, a2, node3, a3, c0);
                return;
            }


            OctreeNode<OctreeVertexData> sub1 = node1.IsLeaf ? node1 : node1[a1];
            OctreeNode<OctreeVertexData> sub2 = node2.IsLeaf ? node2 : node2[a2];
            OctreeNode<OctreeVertexData> sub3 = node3.IsLeaf ? node3 : node3[a3];
            ProcessEdgeAdaptive(sub1, a1, sub2, a2, sub3, a3, c0);

            sub1 = node1.IsLeaf ? node1 : node1[a1 + c0];
            sub2 = node2.IsLeaf ? node2 : node2[a2 + c0];
            sub3 = node3.IsLeaf ? node3 : node3[a3 + c0];
            ProcessEdgeAdaptive(sub1, a1, sub2, a2, sub3, a3, c0);
        }

        private int GetMaxDepthNode(OctreeNode<OctreeVertexData> n1, OctreeNode<OctreeVertexData> n2, OctreeNode<OctreeVertexData> n3, OctreeNode<OctreeVertexData> n4)
        {
            int maxDepth = n1.Depth;
            var maxDepthNode = 1;


            if (n2.Depth > maxDepth)
            {
                maxDepthNode = 2;
                maxDepth = n2.Depth;
            }


            if (n3.Depth > maxDepth)
            {
                maxDepthNode = 3;
                maxDepth = n3.Depth;
            }


            if (n4.Depth > maxDepth)
            {
                maxDepthNode = 4;
            }

            return maxDepthNode;
        }
        
        private void CheckAndGenerateGeometry(OctreeNode<OctreeVertexData> node1, OctreeNode<OctreeVertexData> node2, OctreeNode<OctreeVertexData> node3, OctreeNode<OctreeVertexData> node4, Axis c0)
        {
            float value1;
            float value2;
            c0.ComplementaryAxes(out Axis c1, out Axis c2);
            
            switch (GetMaxDepthNode(node1, node2, node3, node4))
            {
                case 1:
                    value1 = node1.Data[c1 + c2];
                    value2 = node1.Data[c1 + c2 + c0];
                    break;
                case 2:
                    value1 = node2.Data[c2];
                    value2 = node2.Data[c2 + c0];
                    break;
                case 3:
                    value1 = node3.Data[0];
                    value2 = node3.Data[0 + c0];
                    break;
                default:
                    value1 = node4.Data[c1 ];
                    value2 = node4.Data[c1 +  c0];
                    break;
            }


            if (changeOfSignalInEdge(value1, value2))
            {
                AddQuad(node1, node2, node3, node4, value2 >= value1);
            }

            //CalculatedEdgePoints.Add((node1.Origin + node1.Size * 0.5f + node2.Origin + node2.Size * 0.5f + node3.Origin + node3.Size * 0.5f + node4.Origin + node4.Size * 0.5f) * 0.25f);
        }

        private void AddQuad(OctreeNode<OctreeVertexData> node1,
            OctreeNode<OctreeVertexData> node2,
            OctreeNode<OctreeVertexData> node3,
            OctreeNode<OctreeVertexData> node4, bool axisOriented)
        {
            int vertexOriginalIndex = Vertices.Count;

            AddVertices(node1, node2, node3, node4);

            if (axisOriented)
            {
                Triangles.Add(vertexOriginalIndex + 0);
                Triangles.Add(vertexOriginalIndex + 1);
                Triangles.Add(vertexOriginalIndex + 2);

                Triangles.Add(vertexOriginalIndex + 2);
                Triangles.Add(vertexOriginalIndex + 3);
                Triangles.Add(vertexOriginalIndex + 0);
            }
            else
            {
                Triangles.Add(vertexOriginalIndex + 2);
                Triangles.Add(vertexOriginalIndex + 1);
                Triangles.Add(vertexOriginalIndex + 0);

                Triangles.Add(vertexOriginalIndex + 0);
                Triangles.Add(vertexOriginalIndex + 3);
                Triangles.Add(vertexOriginalIndex + 2);
            }
        }

        private void AddVertices(OctreeNode<OctreeVertexData> node1,
            OctreeNode<OctreeVertexData> node2,
            OctreeNode<OctreeVertexData> node3,
            OctreeNode<OctreeVertexData> node4)
        {
            Vertices.Add(WorldToLocal.MultiplyPoint(node1.ResultingVertex()));
            Vertices.Add(WorldToLocal.MultiplyPoint(node2.ResultingVertex()));
            Vertices.Add(WorldToLocal.MultiplyPoint(node3.ResultingVertex()));
            Vertices.Add(WorldToLocal.MultiplyPoint(node4.ResultingVertex()));

        }

        private bool changeOfSignalInEdge(float value1, float value2)
        {
            //to avoid undecided points 0 is considered positive
            return value1 * value2 < 0 || value1 * value2 == 0 && value1 + value2 < 0;
        }

        private void CheckAndGenerateGeometry(OctreeNode<OctreeVertexData> node1, Axis a1,
            OctreeNode<OctreeVertexData> node2, Axis a2,
            OctreeNode<OctreeVertexData> node3, Axis a3, Axis c0)
        {
            return;
            /*
            int maxDepth = node1.Depth;
            var nodeIndex = 1;


            if (node2.Depth > maxDepth)
            {
                nodeIndex = 2;
                maxDepth = node2.Depth;
            }


            if (node3.Depth > maxDepth)
            {
                nodeIndex = 3;
            }


            float value1;
            float value2;

            switch (nodeIndex)
            {
                case 1:
                    value1 = node1.Data[a1];
                    value2 = node1.Data[a1 + c0];
                    break;
                case 2:
                    value1 = node2.Data[a2];
                    value2 = node2.Data[a2 + c0];
                    break;
                default:
                    value1 = node3.Data[a3];
                    value2 = node3.Data[a3 + c0];
                    break;
            }


            if (changeOfSignalInEdge(value1, value2))
            {
                int vertexOriginalIndex = Vertices.Count;
                Vertices.Add(node1.ResultingVertex());
                Vertices.Add(node2.ResultingVertex());
                Vertices.Add(node3.ResultingVertex());

                Triangles.Add(vertexOriginalIndex);
                Triangles.Add(vertexOriginalIndex + 1);
                Triangles.Add(vertexOriginalIndex + 2);
            }
            //CalculatedEdgePoints.Add((node1.Origin + node1.Size * 0.5f + node2.Origin + node2.Size * 0.5f + node3.Origin + node3.Size * 0.5f) * 0.33333f);
            */
        }
    }
}