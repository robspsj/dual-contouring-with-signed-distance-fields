using MeshGeneration.Octree;
using UnityEngine;

namespace MeshGeneration
{
    public class OctreeGizmoDrawer : MonoBehaviour
    {
        [SerializeField] private OctreeMeshGen octreeMeshGen;
        [SerializeField] private OctreeMeshGeneratorProperties octreeMeshGeneratorProperties;

        [SerializeField] private bool drawGizmos;
        [SerializeField] private bool drawCubes;
        [SerializeField] private bool drawBoundingBox;
        [SerializeField] private bool onlyDrawCubesForMaxDepth;
        [SerializeField] private bool drawCurrentProcessedNode;

        private void OnDrawGizmos()
        {
            if (drawBoundingBox)
            {
                Gizmos.color = Color.yellow * 0.5f + Color.blue;
                Gizmos.DrawWireCube(octreeMeshGen.transform.position, octreeMeshGen.transform.localScale);
            }

            if ((drawGizmos || drawCubes) && octreeMeshGen.DistanceFieldOctree != null)
            {
                DrawGizmosRecursive(octreeMeshGen.DistanceFieldOctree.Root, 0);
            }


            if (octreeMeshGen.MeshGenerator != null && drawCurrentProcessedNode)
            {
                DrawCurrentProcessedNode(octreeMeshGen.MeshGenerator);
            }
        }

        private void DrawCurrentProcessedNode(RecursiveOctreePolygonGenerator recursiveOctreePolygonGenerator)
        {
            foreach (OctreeNode<OctreeVertexData> node in recursiveOctreePolygonGenerator.processedNodes)
            {
                switch (recursiveOctreePolygonGenerator.processedNodes.Count)
                {
                    case 1:
                        Gizmos.color = Color.yellow;
                        break;
                    case 2:
                        Gizmos.color = Color.blue;
                        break;
                    case 3:
                    case 4:
                        Gizmos.color = Color.green;
                        break;
                }

                Gizmos.DrawCube(node.Origin + node.Size * 0.5f, node.Size);
                Gizmos.color = Color.Lerp(Color.red, Color.yellow, 0.5f);
                Gizmos.DrawWireCube(node.Origin + node.Size * 0.5f, node.Size);
            }
        }

        private void DrawGizmosRecursive(OctreeNode<OctreeVertexData> node, int depth)
        {
            if (node.IsLeaf)
            {
                DrawGizmosForLeaf(node, depth);
            }
            else
            {
                foreach (OctreeNode<OctreeVertexData> child in node.Children)
                {
                    DrawGizmosRecursive(child, depth + 1);
                }
            }
        }

        private Color ColorForDepth(int depth)
        {
            switch (depth)
            {
                case 0: return Color.white;
                case 1: return Color.green;
                case 2: return Color.yellow;
                case 3: return Color.red;
                case 4: return Color.magenta;
                case 5: return Color.blue;
                default: return Color.black;
            }
        }

        private void DrawGizmosForLeaf(OctreeNode<OctreeVertexData> node, int depth)
        {
            if (drawCubes)
            {
                if (!onlyDrawCubesForMaxDepth || depth == octreeMeshGeneratorProperties.MaxOctreeDepth)
                {
                    Gizmos.color = ColorForDepth(depth);
                    Gizmos.DrawWireCube(node.Origin + node.Size * 0.5f, node.Size * (1 - 0.01f * depth));
                }
            }


            if (drawGizmos)
            {
                for (var iX = 0; iX < 2; iX++)
                for (var iY = 0; iY < 2; iY++)
                for (var iZ = 0; iZ < 2; iZ++)
                {
                    Gizmos.DrawIcon(node.Origin + Vector3.Scale(new Vector3(iX, iY, iZ), node.Size),
                        "blendSampler",
                        true,
                        Color.HSVToRGB(0, 0.2f, Mathf.Clamp(-node.Data[iX, iY, iZ] * 10f + 0.5f, 0, 1)));
                }
            }
        }
    }
}