using UnityEngine;

namespace MeshGeneration
{
    public class OctreeMeshGeneratorProperties : MonoBehaviour
    {
        public SDFObject sdf;
        public int MaxOctreeDepth = 2;
    }
}