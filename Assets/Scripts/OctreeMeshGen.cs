﻿using UnityEngine;


namespace MeshGeneration
{
    public class OctreeMeshGen : MonoBehaviour
    {
        [SerializeField] private OctreeMeshGeneratorProperties octreeMeshGeneratorProperties;

        public Octree.DistanceFieldOctree DistanceFieldOctree;
        public RecursiveOctreePolygonGenerator MeshGenerator;

        public void RegenerateGeometry()
        {
            CalculateGeometry();
            CreateMeshAndSet();
        }


        private void CreateMeshAndSet()
        {
            if (MeshGenerator == null)
            {
                return;
            }

            var meshFilter = GetComponent<MeshFilter>();
            var mesh = new Mesh();
            meshFilter.mesh = mesh;

            mesh.Clear();
            mesh.vertices = MeshGenerator.Vertices.ToArray();
            mesh.triangles = MeshGenerator.Triangles.ToArray();
            mesh.RecalculateNormals();
        }

        private void CalculateGeometry()
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();

            GenerateDistanceFieldOctree();
            Debug.Log($"GenerateDistanceFieldOctree of depth {octreeMeshGeneratorProperties.MaxOctreeDepth} calculated in {watch.ElapsedMilliseconds}ms", this);
            watch.Restart();

            SimplifyTreeWithQEFs();
            //Debug.Log($"SimplifyTreeWithQEFs calculated in {watch.ElapsedMilliseconds} ms");
            //watch.Restart();

            RecursivelyGeneratePolygons();

            Debug.Log($"Geometry Generated with {MeshGenerator.Triangles.Count/3} triangles calculated in {watch.ElapsedMilliseconds}ms", this);
            watch.Stop();
        }

        private void GenerateDistanceFieldOctree()
        {
            DistanceFieldOctree = new Octree.DistanceFieldOctree();
            var transform1 = transform;
            var scale = transform1.lossyScale;
            Vector3 pos = transform1.position;
            DistanceFieldOctree.Populate(octreeMeshGeneratorProperties.MaxOctreeDepth, octreeMeshGeneratorProperties.sdf,
                pos - scale * 0.5f, scale);
        }

        private void SimplifyTreeWithQEFs()
        {
            //throw new System.NotImplementedException();
        }

        private void RecursivelyGeneratePolygons()
        {
            MeshGenerator = new RecursiveOctreePolygonGenerator();
            MeshGenerator.WorldToLocal = transform.worldToLocalMatrix;
            
            MeshGenerator.ProcessCell(DistanceFieldOctree.Root);

            var meshFilter = GetComponent<MeshFilter>();
            var mesh = new Mesh();
            meshFilter.mesh = mesh;

            mesh.Clear();
            mesh.vertices = MeshGenerator.Vertices.ToArray();
            mesh.triangles = MeshGenerator.Triangles.ToArray();
            mesh.RecalculateNormals();
        }
    }
}