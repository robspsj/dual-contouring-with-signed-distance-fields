﻿using UnityEngine;


namespace MeshGeneration.Octree
{
    public class OctreeNode<TData>
    {
        public OctreeNode<TData>[] Children;

        public TData Data;
        public bool IsLeaf => Children == null;

        public readonly Vector3 Origin;
        public readonly Vector3 Size;
        public readonly int Depth;

        public float HalfLongestLenght;

        public OctreeNode(TData data, Vector3 origin, Vector3 size, int depth)
        {
            Data = data;
            Origin = origin;
            Size = size;
            Depth = depth;

            HalfLongestLenght = Mathf.Max(size.x, Mathf.Max(size.y, size.z))/2;
        }

        public OctreeNode<TData> this[int indexX, int indexY, int indexZ]
        {
            get => Children[indexX + indexY * 2 + indexZ * 4];
            set => Children[indexX + indexY * 2 + indexZ * 4] = value;
        }
        
        public OctreeNode<TData> this[int i]
        {
            get => Children[i];
            set => Children[i] = value;
        }

        public Vector3 ResultingVertex()
        {
            return Origin + Size * 0.5f;
        }
        
    }
}