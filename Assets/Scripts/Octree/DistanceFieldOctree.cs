﻿using System;
using UnityEngine;

namespace MeshGeneration.Octree
{
    public class DistanceFieldOctree : Octree<OctreeVertexData>
    {
        public void Populate(int maxDepth, SDFObject sdf, Vector3 origin, Vector3 size)
        {
            var rootData = new OctreeVertexData();

            for (var iX = 0; iX < 2; iX++)
            for (var iY = 0; iY < 2; iY++)
            for (var iZ = 0; iZ < 2; iZ++)
            {
                rootData[iX + 2 * iY + 4 * iZ] =
                    sdf.GetPointValue(origin + Vector3.Scale(size, new Vector3(iX, iY, iZ)));
            }


            Root = new OctreeNode<OctreeVertexData>(rootData, origin, size, 0);

            FillNodesRecursive(Root, maxDepth, sdf);
            SimplifyRecursive(Root);
        }

        private void GetSubVertexCacheForNodeNoAlloc(OctreeNode<OctreeVertexData> node, SDFObject sdf, ref Span<float> subVertexCache)
        {
            for (var ix = 0; ix < 3; ix++)
            for (var iy = 0; iy < 3; iy++)
            for (var iz = 0; iz < 3; iz++)
            {
                int i = iz*9 + iy * 3 + ix;

                if (ix != 1 && iy != 1 && iz != 1)
                {
                    subVertexCache[i] = node.Data[ix/2, iy/2, iz/2];
                }
                else
                {
                    subVertexCache[i] =
                        sdf.GetPointValue(node.Origin + Vector3.Scale(node.Size, new Vector3(ix, iy, iz)) * 0.5f);
                }
            }
        }

        private void FillNodeWithChildren(OctreeNode<OctreeVertexData> node, SDFObject sdf)
        {
            
            Span<float> subVertexCache = stackalloc float[27];
            GetSubVertexCacheForNodeNoAlloc(node, sdf, ref subVertexCache);

            node.Children = new OctreeNode<OctreeVertexData>[8];

            for (var ix = 0; ix < 2; ix++)
            for (var iy = 0; iy < 2; iy++)
            for (var iz = 0; iz < 2; iz++)
            {
                var childVertexValues = new OctreeVertexData();

                for (var jx = 0; jx < 2; jx++)
                for (var jy = 0; jy < 2; jy++)
                for (var jz = 0; jz < 2; jz++)
                {
                    int k = 9* (jz + iz) + 3 * (jy + iy) + 1 * (jx + ix);
                    childVertexValues[jx, jy, jz] = subVertexCache[k];
                }


                node[ix, iy, iz] = new OctreeNode<OctreeVertexData>(childVertexValues,
                    node.Origin + Vector3.Scale(node.Size, new Vector3(ix, iy, iz)) * 0.5f, node.Size * 0.5f, node.Depth + 1);
            }
        }

        private bool CanPruneEarly(OctreeNode<OctreeVertexData> node)
        {
            return node.Data.SureIsEmpty(node.HalfLongestLenght) || node.Data.SureIsSolid(node.HalfLongestLenght);
        }


        private void FillNodesRecursive(OctreeNode<OctreeVertexData> node, int maxDepth, SDFObject sdf)
        {
            if (node.Depth >= maxDepth)
            {
                return;
            }


            if (CanPruneEarly(node))
            {
                return;
            }


            FillNodeWithChildren(node, sdf);

            for (var i = 0; i < 8; i++)
            {
                FillNodesRecursive(node[i], maxDepth, sdf);
            }
        }

        private void SimplifyRecursive(OctreeNode<OctreeVertexData> node)
        {
            if (node.IsLeaf)
            {
                return;
            }


            foreach (var childNode in node.Children)
            {
                SimplifyRecursive(childNode);
            }


            var changeInChildren = false;

            foreach (OctreeNode<OctreeVertexData> childNode in node.Children)
            {
                if (childNode.IsLeaf && (childNode.Data.IsPositive() || childNode.Data.IsNegative()))
                {
                    continue;
                }


                changeInChildren = true;
                break;
            }


            if (!changeInChildren)
            {
                node.Children = null;
            }
        }
    }
}