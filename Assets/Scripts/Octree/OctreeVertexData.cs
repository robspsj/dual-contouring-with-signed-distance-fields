﻿using System.Collections;
using System.Collections.Generic;


namespace MeshGeneration.Octree
{
    public struct OctreeVertexData : IEnumerable<float>
    {
        private float v0;
        private float v1;
        private float v2;
        private float v3;
        private float v4;
        private float v5;
        private float v6;
        private float v7;

        public float this[int index]
        {
            get
            {
                return index switch
                {
                    0 => v0,
                    1 => v1,
                    2 => v2,
                    3 => v3,
                    4 => v4,
                    5 => v5,
                    6 => v6,
                    7 => v7,
                    _ => 0
                };
            }
            set
            {
                switch (index)
                {
                    case 0:
                        v0 = value;
                        break;
                    case 1:
                        v1 = value;
                        break;
                    case 2:
                        v2 = value;
                        break;
                    case 3:
                        v3 = value;
                        break;
                    case 4:
                        v4 = value;
                        break;
                    case 5:
                        v5 = value;
                        break;
                    case 6:
                        v6 = value;
                        break;
                    case 7:
                        v7 = value;
                        break;
                }
            }
        }

        public float this[int indexX, int indexY, int indexZ] 
        {
            get =>  this[indexX + indexY * 2 + indexZ * 4];
            set => this[indexX + indexY * 2 + indexZ * 4] = value;
        }
        
        public float Min()
        {
            float min = v0;

            if (v1 < min) min = v1;
            if (v2 < min) min = v2;
            if (v3 < min) min = v3;
            if (v4 < min) min = v4;
            if (v5 < min) min = v5;
            if (v6 < min) min = v6;
            if (v7 < min) min = v7;

            return min;
        }
        
        
        public float Max()
        {
            float max = v0;

            if (v1 > max) max = v1;
            if (v2 > max) max = v2;
            if (v3 > max) max = v3;
            if (v4 > max) max = v4;
            if (v5 > max) max = v5;
            if (v6 > max) max = v6;
            if (v7 > max) max = v7;

            return max;
        }
        
        public IEnumerator<float> GetEnumerator()
        {
            yield return v0;
            yield return v1;
            yield return v2;
            yield return v3;
            yield return v4;
            yield return v5;
            yield return v6;
            yield return v7;
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        
        public bool IsPositive()
        {
            return Min() >= 0;
        }
        public bool IsNegative()
        {
            return Max() < 0;
        }
        
        public bool SureIsSolid(float halfLongestLength)
        {

            foreach (float value in this)
            {
                if (value <= halfLongestLength)
                {
                    return false;
                }
            }
            
            return true;
        }
        public bool SureIsEmpty(float halfLongestLength)
        {
            foreach (float value in this)
            {
                if (value > -halfLongestLength)
                {
                    return false;
                }
            }
            
            return true;
        }
    }
}