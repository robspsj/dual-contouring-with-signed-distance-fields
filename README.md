# Dual Contouring With signed Distance Fieldw
## Introduction
This repo contain my studies and implementation of Dual Contouring of Hermite Data](https://www.cs.rice.edu/~jwarren/papers/dualcontour.pdf)

## Future Tasks

### SDF definition
- [x] shape intersection SDF
- [x] Create SDF Interface
- [x] SDF Circle
- [x] pseudo SDF Perlin Noise
- [ ] sdf 3d texture
- [ ] logic tree SDF

### Tools
- [x] processing gizmos
- [x] vertex gizmo tool

### MeshGen
- [x] Grid Contouring
- [ ] Adaptive Octree Grid Contouring
  - [x] Octree Generation
    - [ ] async implementation
    - [x] early tree pruning
    - [x] late tree simplification
  - [x] Octree Traversal and recursive filling
    - [ ] async implementation
    - [ ] Partial traversal based on tree bias
  - [ ] in cell vertex positioning
    - [ ]cell center bias

### Extras
- [ ] Volumetric material
- [ ] Multiple Material information in octree
- [ ] Mesh Editing
- [ ] Mesh Recalculation